package com.radoman;
public class BlackjackCard extends Card {

    public int value;

    public int get_value(){
        setValue();

        return this.value;
    }

    public void setValue(){
        switch(this.number){
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:  this.value = this.number; break;
            case 11: this.value = 1; break;
            case 12:
            case 13:
            case 14: this.value = 10; break;
        }
    }
}
